var url = require("url");

var osmosis = require('osmosis');
var low = require('lowdb');

var db = low('db.json');

osmosis
    .get('http://www.altaviadeimontiliguri.it/meteocamsrv/cam.htm?postazione=antola')
    .find('url-immagine1')
    .set('url')
    .data(function(listing) {
	if (db('images').find({ url: 'http://www.altaviadeimontiliguri.it/' + listing['url'] })) {
	    console.log('already here!');
	} else {
	    db('images').push({ url: 'http://www.altaviadeimontiliguri.it/' + listing['url']});}
    })
